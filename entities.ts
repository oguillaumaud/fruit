export interface Fruits {
    id?: number,
    image: string,
    prixHT: number,
    nom: string,
    quantite:number
}